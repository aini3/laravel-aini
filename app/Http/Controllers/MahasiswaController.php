<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function index()
    {
        $mhs = Mahasiswa::all();
        return view('mahasiswa.index',[
            'mhs' => $mhs,
        ]);
    }

    public function create()
    {
        return view('mahasiswa.form');
    }

    public function store(Request $request)
    {
        $mhs = new Mahasiswa();
        $mhs->nim = $request->nim;
        $mhs->nama = $request->nama;
        $mhs->save();

        return redirect()->route("mhs::index");
    }

    public function show(Mahasiswa $mhs)
    {
        return view('mahasiswa.show',[
            'mhs' => $mhs,
        ]);
    }
}
