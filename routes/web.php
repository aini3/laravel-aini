<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[\App\Http\Controllers\MahasiswaController::class,'index'])->name('mhs::index');
Route::get('/create',[\App\Http\Controllers\MahasiswaController::class,'create'])->name('mhs::create');
Route::post('/create',[\App\Http\Controllers\MahasiswaController::class,'store'])->name('mhs::store');
Route::get('/{mhs}',[\App\Http\Controllers\MahasiswaController::class,'show'])->name('mhs::show');
