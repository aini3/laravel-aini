<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">    
    <title>Document</title>
</head>
<body>
<form action="{{route("mhs::store")}}" method="post">
    @csrf
    <label for="nim"><b>NIM : </b></label>
    <input type="number" placeholder="NIM" name="nim" id="nim" required><br>
    <label for="nama"><b>Nama : </b></label>
    <input type="text" placeholder="Nama" name="nama" id="nama" required><br>
    <button type="submit">Tambah</button>
</form>
</body>
</html>
