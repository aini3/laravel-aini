<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>    
</head>
<body>
    <h4>Data Mahasiswa</h4>
    <table border="1">
        <thead style="font-weight: bold">
            <tr>
                <td>ID</td>
                <td>NIM</td>
                <td>Nama</td>
                <td>Created at</td>
                <td>Updated at</td>
                <td>Aksi</td>
              </tr>
        </thead>
        <tbody>
            @forelse ($mhs as $mahasiswa)
            <tr>
                <td>{{ $mahasiswa->id }}</td>
                <td>{{ $mahasiswa->nim }}</td>
                <td>{{ $mahasiswa->nama }}</td>
                <td>{{ $mahasiswa->created_at }}</td>
                <td>{{ $mahasiswa->updated_at }}</td>
                <td><a href="{{route("mhs::show",$mahasiswa)}}">lihat</a></td>
              </tr>
            @empty
            <tr>
                <td colspan="6" style="text-align: center">Belum ada data.</td>
              </tr>
            @endforelse
        </tbody>
      </table>
      <a href="{{route("mhs::create")}}">Tambah mahasiswa</a>
</body>
</html>
